package com.tom.view;

public interface MenuItemListener {
	public abstract void onAction(); 
}
