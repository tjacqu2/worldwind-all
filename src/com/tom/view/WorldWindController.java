package com.tom.view;

import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.tom.application.MainApp;
import com.tom.view.MasterTree.TreeRoot;
import com.tom.view.MasterTree.TreeTeams;
import gov.nasa.worldwind.BasicModel;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.awt.WorldWindowGLJPanel;
import gov.nasa.worldwindx.examples.SimplestPossibleExample;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;


public class WorldWindController implements Initializable {
	
	@FXML
	public VBox vbox;
	@FXML
	public StackPane sp2;
	
	private MainApp mainApp;
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		TreeItem<String> playersRoot = new TreeItem<String>("Players");
		playersRoot.setExpanded(true);
		MasterTree mt = new MasterTree();
		TreeRoot players = mt.new TreeRoot("Players");
		TreeTeams team1 = mt.new TreeTeams("Team 1");
		TreeTeams team2 = mt.new TreeTeams("Team 2");
			
		//TreeItem<String> item5 = new TreeItem<String>("Speed");
		//TreeItem<String> item6 = new TreeItem<String>("Lat");
		//TreeItem<String> item7 = new TreeItem<String>("lon");
		//TreeItem<String> item8 = new TreeItem<String>("Alt");
		playersRoot.getChildren().add(players);
		players.getChildren().add(team1);
		players.getChildren().add(team2);
		
		
		TreeView<String> treeView = new TreeView<String>(playersRoot);
		treeView.setShowRoot(false);
		//treeView.setEditable(true);
		treeView.setCellFactory(new Callback<TreeView<String>,TreeCell<String>>(){
	        @Override
	        public TreeCell<String> call(TreeView<String> p) {
	            return mt.new TreeCellContextMenu();
	        }
	    });
		 
		MenuBar menuBar = new MenuBar();
		
		//MenuItem to set listener on
		MenuItem newScenario = new MenuItem("New");
		
		final Menu menu1 = new Menu("File");
		final Menu menu2 = new Menu("Options");
		menu1.getItems().add(newScenario);
		menuBar.getMenus().addAll(menu1, menu2);
		vbox.getChildren().addAll(menuBar, treeView);
		
		
		//add menu items to list here
		
		
		//MyContextMenus level1 = new MyContextMenus();
		//level1.getAddPlayer();
		
		WorldWindowGLJPanel wwd = new WorldWindowGLJPanel();
	    wwd.setPreferredSize(new java.awt.Dimension(400, 400));
	    
	    wwd.setModel(new BasicModel());
	    //JFrame frame = new JFrame();
	    JPanel jp = new JPanel();	    
	    jp.add(wwd);
	    SwingNode sn = new SwingNode();
	    //sn.getContent().set
	    sn.setContent(jp);
	    sp2.getChildren().add(sn);
	    //frame.getContentPane().add(wwd, java.awt.BorderLayout.CENTER);
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.pack();
        //frame.setVisible(true);
	    
	    //spe.runWorld();
	    
		/*
		
        SwingNode sn = new SwingNode();
        
        wwd.setModel(new BasicModel());*/
        
		//SimplestPossibleExample spe = new SimplestPossibleExample();
		//sp2.getChildren().add(spe.runWorld());
		
		//register listener on MenuItem
		newScenario.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent ae) {
				PlayerScenarioListener  p = new PlayerScenarioListener();
				p.onAction();
			}
		});
		
		
	}
	
	
	public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
	}
		
		
}
