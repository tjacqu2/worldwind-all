package com.tom.view;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;

/*
 * Class with nested classes to define the TreeView levels and create separate context menus for each level
 */

public class MasterTree {
	
	public abstract class AbstractTreeColumn extends TreeItem<String>{
	    public abstract ContextMenu getContextMenu();
	}
	
public class TreeRoot extends AbstractTreeColumn {
		
		public TreeRoot(String teamName) {
			this.setValue(teamName);
		}

		@Override
		public ContextMenu getContextMenu() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	public class TreeTeams extends AbstractTreeColumn {
		
		public TreeTeams(String teamName) {
			this.setValue(teamName);
		}
		@Override
		public ContextMenu getContextMenu() {
			// TODO Auto-generated method stub
			MyContextMenus add = new MyContextMenus();
			add.getAddPlayer().setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					// TODO Auto-generated method stub
					System.out.println("test");
				}
			});
			return new ContextMenu(add.getAddPlayer());
		}
		
	}
	
	public class TreePlayers extends AbstractTreeColumn {

		public TreePlayers(String players) {
			this.setValue(players);
		}
		
		@Override
		public ContextMenu getContextMenu() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	public class TreeAttributes extends AbstractTreeColumn {
		
		public TreeAttributes(String attributes ) {
			this.setValue(attributes);
		}

		@Override
		public ContextMenu getContextMenu() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	public class TreeCellContextMenu extends TreeCell<String> {
		
		 @Override
		    public void updateItem(String item, boolean empty) {
		        super.updateItem(item, empty);

		        if (empty) {
		            setText(null);
		            setGraphic(null);
		        } else {
		            setText(getItem() == null ? "" : getItem().toString());
		            setGraphic(getTreeItem().getGraphic());
		            setContextMenu(((AbstractTreeColumn) getTreeItem()).getContextMenu());
		        }
		    }
	}

}
