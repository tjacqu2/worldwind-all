package com.tom.view;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;

public class MyContextMenus {
	private final ContextMenu contextMenu;
	private final MenuItem addPlayer;
	private final MenuItem deletePlayer;
	private final MenuItem editPlayer;
	private final MenuItem editPlayerRoute;
	//private ObservableList<MenuItem> cmList = FXCollections.observableArrayList();
	
	public MyContextMenus() {
		this.addPlayer = new MenuItem("Add Player");
		this.deletePlayer = new MenuItem("Delete Player");
		this.editPlayer =new MenuItem("Edit Player");
		this.editPlayerRoute = new MenuItem("Edit Player Route");
		this.contextMenu = new ContextMenu(addPlayer, deletePlayer, editPlayer, editPlayerRoute);
	}
	
	public ContextMenu getContextMenu() {
		return contextMenu;
	}
	
	public MenuItem getAddPlayer() {
		return addPlayer;
	}
	
	public MenuItem getDeletePlayer() {
		return deletePlayer;
	}
	
	public MenuItem getEditPlayer() {
		return editPlayer;
	}
	
	public MenuItem getEditPlayerRoute() {
		return editPlayerRoute;
	}
	
	public void freeActionListeners() {
		this.addPlayer.setOnAction(null);
		this.deletePlayer.setOnAction(null);
		this.editPlayer.setOnAction(null);
		this.editPlayerRoute.setOnAction(null);
	}
}
