package com.tom.application;
	
import java.io.IOException;

//import com.tom.application.MainApp;
import com.tom.view.WorldWindController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;


public class MainApp extends Application {
	
	private Stage primaryStage;
	private BorderPane rootPane;
	
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("WorldWind");
		
		initRootLayout();
		showWorldWind();
		
	}
	
	public void initRootLayout() {
		try {
			
			FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/com/tom/view/rootLayout.fxml"));
            rootPane = (BorderPane) loader.load();
            Scene scene = new Scene(rootPane);
            //scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
            
						
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void showWorldWind() {
		try {
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("/com/tom/view/worldWindLayout.fxml"));
        SplitPane worldWindPane = (SplitPane) loader.load();
        rootPane.setCenter(worldWindPane);
        
        // Give the controller access to the main app.
        WorldWindController controller = loader.getController();
        controller.setMainApp(this);
		
		} catch(IOException e) {
			e.printStackTrace();
			
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
